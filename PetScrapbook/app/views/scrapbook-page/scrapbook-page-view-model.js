"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var ScrapBookPageModel = (function (_super) {
    __extends(ScrapBookPageModel, _super);
    function ScrapBookPageModel() {
        var _this = _super.call(this) || this;
        _this._listItems = [];
        return _this;
        //  this._listItems.push( "Item1");
        //  this._listItems.push( "Item2");
        //  this._listItems.push( "Item3");
    }
    ScrapBookPageModel.prototype.init = function () {
        //  items : Array<ByteString>;
        this._listItems.push("Item1");
        this._listItems.push("Item2");
        this._listItems.push("Item3");
    };
    ScrapBookPageModel.prototype.getItems = function () {
        return this._listItems;
    };
    return ScrapBookPageModel;
}(observable_1.Observable));
exports.ScrapBookPageModel = ScrapBookPageModel;
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NyYXBib29rLXBhZ2Utdmlldy1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNjcmFwYm9vay1wYWdlLXZpZXctbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSw4Q0FBMkg7QUFPM0g7SUFBd0Msc0NBQVU7SUFJOUM7UUFBQSxZQUNLLGlCQUFPLFNBT1g7UUFOSSxLQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQzs7UUFFdEIsbUNBQW1DO1FBQ25DLG1DQUFtQztRQUNuQyxtQ0FBbUM7SUFFdkMsQ0FBQztJQUdNLGlDQUFJLEdBQVg7UUFDQSw4QkFBOEI7UUFFMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUUsT0FBTyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUUsT0FBTyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUUsT0FBTyxDQUFDLENBQUM7SUFFbkMsQ0FBQztJQUVNLHFDQUFRLEdBQWY7UUFDSSxNQUFNLENBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUM1QixDQUFDO0lBS0wseUJBQUM7QUFBRCxDQUFDLEFBL0JELENBQXdDLHVCQUFVLEdBK0JqRDtBQS9CWSxnREFBa0I7QUErQjlCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb21PYmplY3QsIGZyb21PYmplY3RSZWN1cnNpdmUsIFByb3BlcnR5Q2hhbmdlRGF0YSwgRXZlbnREYXRhLCBXcmFwcGVkVmFsdWUgfSBmcm9tIFwiZGF0YS9vYnNlcnZhYmxlXCI7XG5cblxuXG5cblxuXG5leHBvcnQgY2xhc3MgU2NyYXBCb29rUGFnZU1vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZSB7XG5cbiAgICBfbGlzdEl0ZW1zIDogQXJyYXk8U3RyaW5nPjtcbiAgIFxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgIHRoaXMuX2xpc3RJdGVtcyA9IFtdO1xuXG4gICAgICAgIC8vICB0aGlzLl9saXN0SXRlbXMucHVzaCggXCJJdGVtMVwiKTtcbiAgICAgICAgLy8gIHRoaXMuX2xpc3RJdGVtcy5wdXNoKCBcIkl0ZW0yXCIpO1xuICAgICAgICAvLyAgdGhpcy5fbGlzdEl0ZW1zLnB1c2goIFwiSXRlbTNcIik7XG5cbiAgICB9XG4gICAgXG5cbiAgICBwdWJsaWMgaW5pdCgpIHtcbiAgICAvLyAgaXRlbXMgOiBBcnJheTxCeXRlU3RyaW5nPjtcblxuICAgICAgICB0aGlzLl9saXN0SXRlbXMucHVzaCggXCJJdGVtMVwiKTtcbiAgICAgICAgdGhpcy5fbGlzdEl0ZW1zLnB1c2goIFwiSXRlbTJcIik7XG4gICAgICAgIHRoaXMuX2xpc3RJdGVtcy5wdXNoKCBcIkl0ZW0zXCIpO1xuXG4gICAgfVxuXG4gICAgcHVibGljIGdldEl0ZW1zKCkge1xuICAgICAgICByZXR1cm4gIHRoaXMuX2xpc3RJdGVtcztcbiAgICB9XG5cblxuXG5cbn07Il19