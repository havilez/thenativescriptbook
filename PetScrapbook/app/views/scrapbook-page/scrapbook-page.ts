



import { Observable, fromObject, fromObjectRecursive, PropertyChangeData, EventData, WrappedValue } from "data/observable";

import { Page } from 'ui/page';
import { ListPicker } from "ui/list-picker";

import { ScrapBookPageModel } from './scrapbook-page-view-model';

// instantiate page  model
var vm = new ScrapBookPageModel();


exports.onLoaded = function(args :EventData) {
    
    let page = <Page>args.object;
    

    page.bindingContext = new ScrapBookPageModel();

};
