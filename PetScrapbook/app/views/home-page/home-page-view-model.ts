import {Observable} from 'data/observable';

export class HomePageModel extends Observable {

    private _counter: number;
    private _message: string;

    private _header :string;
    private _footer :string;

    constructor() {
        super();

        this.init();
   
    }

    public init() {

        // Initialize default values.
        this._header = "Pet Scrapbook",
        this._footer = "Brosteins ©2016";
    }


    get footer(): string {
        return this._footer;
    }

    set footer( value: string) {
        this._footer = value;
    }

    get header(): string {
        return this._header;
    }

    set header( value: string) {
        this._header = value;
    }




    get message(): string {
        return this._message;
    }
    
    set message(value: string) {
        if (this._message !== value) {
            this._message = value;
            this.notifyPropertyChange('message', value)
        }
    }

    

   
}