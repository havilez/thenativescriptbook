"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var HomePageModel = (function (_super) {
    __extends(HomePageModel, _super);
    function HomePageModel() {
        var _this = _super.call(this) || this;
        _this.init();
        return _this;
    }
    HomePageModel.prototype.init = function () {
        // Initialize default values.
        this._header = "Pet Scrapbook",
            this._footer = "Brosteins ©2016";
    };
    Object.defineProperty(HomePageModel.prototype, "footer", {
        get: function () {
            return this._footer;
        },
        set: function (value) {
            this._footer = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HomePageModel.prototype, "header", {
        get: function () {
            return this._header;
        },
        set: function (value) {
            this._header = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HomePageModel.prototype, "message", {
        get: function () {
            return this._message;
        },
        set: function (value) {
            if (this._message !== value) {
                this._message = value;
                this.notifyPropertyChange('message', value);
            }
        },
        enumerable: true,
        configurable: true
    });
    return HomePageModel;
}(observable_1.Observable));
exports.HomePageModel = HomePageModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS1wYWdlLXZpZXctbW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLXBhZ2Utdmlldy1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDhDQUEyQztBQUUzQztJQUFtQyxpQ0FBVTtJQVF6QztRQUFBLFlBQ0ksaUJBQU8sU0FJVjtRQUZHLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7SUFFaEIsQ0FBQztJQUVNLDRCQUFJLEdBQVg7UUFFSSw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxlQUFlO1lBQzlCLElBQUksQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUM7SUFDckMsQ0FBQztJQUdELHNCQUFJLGlDQUFNO2FBQVY7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDO2FBRUQsVUFBWSxLQUFhO1lBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLENBQUM7OztPQUpBO0lBTUQsc0JBQUksaUNBQU07YUFBVjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7YUFFRCxVQUFZLEtBQWE7WUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDekIsQ0FBQzs7O09BSkE7SUFTRCxzQkFBSSxrQ0FBTzthQUFYO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQzthQUVELFVBQVksS0FBYTtZQUNyQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFBO1lBQy9DLENBQUM7UUFDTCxDQUFDOzs7T0FQQTtJQVlMLG9CQUFDO0FBQUQsQ0FBQyxBQXhERCxDQUFtQyx1QkFBVSxHQXdENUM7QUF4RFksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge09ic2VydmFibGV9IGZyb20gJ2RhdGEvb2JzZXJ2YWJsZSc7XG5cbmV4cG9ydCBjbGFzcyBIb21lUGFnZU1vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZSB7XG5cbiAgICBwcml2YXRlIF9jb3VudGVyOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfbWVzc2FnZTogc3RyaW5nO1xuXG4gICAgcHJpdmF0ZSBfaGVhZGVyIDpzdHJpbmc7XG4gICAgcHJpdmF0ZSBfZm9vdGVyIDpzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcblxuICAgICAgICB0aGlzLmluaXQoKTtcbiAgIFxuICAgIH1cblxuICAgIHB1YmxpYyBpbml0KCkge1xuXG4gICAgICAgIC8vIEluaXRpYWxpemUgZGVmYXVsdCB2YWx1ZXMuXG4gICAgICAgIHRoaXMuX2hlYWRlciA9IFwiUGV0IFNjcmFwYm9va1wiLFxuICAgICAgICB0aGlzLl9mb290ZXIgPSBcIkJyb3N0ZWlucyDCqTIwMTZcIjtcbiAgICB9XG5cblxuICAgIGdldCBmb290ZXIoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Zvb3RlcjtcbiAgICB9XG5cbiAgICBzZXQgZm9vdGVyKCB2YWx1ZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuX2Zvb3RlciA9IHZhbHVlO1xuICAgIH1cblxuICAgIGdldCBoZWFkZXIoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2hlYWRlcjtcbiAgICB9XG5cbiAgICBzZXQgaGVhZGVyKCB2YWx1ZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuX2hlYWRlciA9IHZhbHVlO1xuICAgIH1cblxuXG5cblxuICAgIGdldCBtZXNzYWdlKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl9tZXNzYWdlO1xuICAgIH1cbiAgICBcbiAgICBzZXQgbWVzc2FnZSh2YWx1ZTogc3RyaW5nKSB7XG4gICAgICAgIGlmICh0aGlzLl9tZXNzYWdlICE9PSB2YWx1ZSkge1xuICAgICAgICAgICAgdGhpcy5fbWVzc2FnZSA9IHZhbHVlO1xuICAgICAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZSgnbWVzc2FnZScsIHZhbHVlKVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgXG5cbiAgIFxufSJdfQ==