
import *  as FrameModule from  "ui/frame";

export function onTap_About() {

    // "ui/frame" NavigationEntry interface
    var navigationEntry = {
        moduleName: "views/about/about",
        transition:  {
            name: "fade"
        }
    };
   
    FrameModule.topmost().navigate(navigationEntry);

}

export function onTap_Products() {
    
        // "ui/frame" NavigationEntry interface
        var navigationEntry = {
            moduleName: "views/products/products",
            transition:  {
                name: "fade"
            }
        };
       
        FrameModule.topmost().navigate(navigationEntry);
    
}

export function onTap_Contacts() {
    
        // "ui/frame" NavigationEntry interface
        var navigationEntry = {
            moduleName: "views/contact-us/contact-us",
            transition:  {
                name: "fade"
            }
        };
       
        FrameModule.topmost().navigate(navigationEntry);
    
}



