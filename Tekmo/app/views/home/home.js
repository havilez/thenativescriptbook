"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FrameModule = require("ui/frame");
function onTap_About() {
    // "ui/frame" NavigationEntry interface
    var navigationEntry = {
        moduleName: "views/about/about",
        transition: {
            name: "fade"
        }
    };
    FrameModule.topmost().navigate(navigationEntry);
}
exports.onTap_About = onTap_About;
function onTap_Products() {
    // "ui/frame" NavigationEntry interface
    var navigationEntry = {
        moduleName: "views/products/products",
        transition: {
            name: "fade"
        }
    };
    FrameModule.topmost().navigate(navigationEntry);
}
exports.onTap_Products = onTap_Products;
function onTap_Contacts() {
    // "ui/frame" NavigationEntry interface
    var navigationEntry = {
        moduleName: "views/contact-us/contact-us",
        transition: {
            name: "fade"
        }
    };
    FrameModule.topmost().navigate(navigationEntry);
}
exports.onTap_Contacts = onTap_Contacts;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhvbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxzQ0FBMEM7QUFFMUM7SUFFSSx1Q0FBdUM7SUFDdkMsSUFBSSxlQUFlLEdBQUc7UUFDbEIsVUFBVSxFQUFFLG1CQUFtQjtRQUMvQixVQUFVLEVBQUc7WUFDVCxJQUFJLEVBQUUsTUFBTTtTQUNmO0tBQ0osQ0FBQztJQUVGLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7QUFFcEQsQ0FBQztBQVpELGtDQVlDO0FBRUQ7SUFFUSx1Q0FBdUM7SUFDdkMsSUFBSSxlQUFlLEdBQUc7UUFDbEIsVUFBVSxFQUFFLHlCQUF5QjtRQUNyQyxVQUFVLEVBQUc7WUFDVCxJQUFJLEVBQUUsTUFBTTtTQUNmO0tBQ0osQ0FBQztJQUVGLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7QUFFeEQsQ0FBQztBQVpELHdDQVlDO0FBRUQ7SUFFUSx1Q0FBdUM7SUFDdkMsSUFBSSxlQUFlLEdBQUc7UUFDbEIsVUFBVSxFQUFFLDZCQUE2QjtRQUN6QyxVQUFVLEVBQUc7WUFDVCxJQUFJLEVBQUUsTUFBTTtTQUNmO0tBQ0osQ0FBQztJQUVGLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7QUFFeEQsQ0FBQztBQVpELHdDQVlDIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgKiAgYXMgRnJhbWVNb2R1bGUgZnJvbSAgXCJ1aS9mcmFtZVwiO1xuXG5leHBvcnQgZnVuY3Rpb24gb25UYXBfQWJvdXQoKSB7XG5cbiAgICAvLyBcInVpL2ZyYW1lXCIgTmF2aWdhdGlvbkVudHJ5IGludGVyZmFjZVxuICAgIHZhciBuYXZpZ2F0aW9uRW50cnkgPSB7XG4gICAgICAgIG1vZHVsZU5hbWU6IFwidmlld3MvYWJvdXQvYWJvdXRcIixcbiAgICAgICAgdHJhbnNpdGlvbjogIHtcbiAgICAgICAgICAgIG5hbWU6IFwiZmFkZVwiXG4gICAgICAgIH1cbiAgICB9O1xuICAgXG4gICAgRnJhbWVNb2R1bGUudG9wbW9zdCgpLm5hdmlnYXRlKG5hdmlnYXRpb25FbnRyeSk7XG5cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG9uVGFwX1Byb2R1Y3RzKCkge1xuICAgIFxuICAgICAgICAvLyBcInVpL2ZyYW1lXCIgTmF2aWdhdGlvbkVudHJ5IGludGVyZmFjZVxuICAgICAgICB2YXIgbmF2aWdhdGlvbkVudHJ5ID0ge1xuICAgICAgICAgICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9wcm9kdWN0cy9wcm9kdWN0c1wiLFxuICAgICAgICAgICAgdHJhbnNpdGlvbjogIHtcbiAgICAgICAgICAgICAgICBuYW1lOiBcImZhZGVcIlxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgIFxuICAgICAgICBGcmFtZU1vZHVsZS50b3Btb3N0KCkubmF2aWdhdGUobmF2aWdhdGlvbkVudHJ5KTtcbiAgICBcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG9uVGFwX0NvbnRhY3RzKCkge1xuICAgIFxuICAgICAgICAvLyBcInVpL2ZyYW1lXCIgTmF2aWdhdGlvbkVudHJ5IGludGVyZmFjZVxuICAgICAgICB2YXIgbmF2aWdhdGlvbkVudHJ5ID0ge1xuICAgICAgICAgICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9jb250YWN0LXVzL2NvbnRhY3QtdXNcIixcbiAgICAgICAgICAgIHRyYW5zaXRpb246ICB7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJmYWRlXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICBcbiAgICAgICAgRnJhbWVNb2R1bGUudG9wbW9zdCgpLm5hdmlnYXRlKG5hdmlnYXRpb25FbnRyeSk7XG4gICAgXG59XG5cblxuXG4iXX0=