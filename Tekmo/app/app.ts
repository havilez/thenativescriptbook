/*
In NativeScript, the app.js file is the entry point to your application.
You can use this file to perform app-level initialization, but the primary
purpose of the file is to pass control to the app’s first module.
*/

require("./bundle-config");
import *  as  application from  "application";


// application.start({ moduleName: "views/products/products" });
// application.start({ moduleName: "views/contact-us/contact-us" });
  application.start({ moduleName: "views/home/home" });
//  application.start({ moduleName: "views/about/about" });

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
